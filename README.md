### N3RDP0RT4L HQ

Welcome to the N3RDP0RT4L headquarters operating on the planet Kepler-186F!

We have achieved many application ports, and modifications to make the SM-T380 better then ever before.

We also hope you can all stick around for the exciting future of the inclusive N3RDP0RT4L scene.

- 📱 We finished iShitOS; A Super-Fast and debloated stock ROM edit for the SM-T380.
- 🧑‍💻 The first working LineageOS build for the SM-T380 has been released.
- 💻 We're currently working on a stock rom edit for the codename "enuma" device.
- 🌐 We have our own website.
- 🍎 Fun fact: Our founder likes MacOS.

You can check our our websites below for more information.<br>
https://n3rdp0rt4l-hq.gitlab.io/n3rdp0rt4l/<br>
https://github.com/N3RDP0RT4L-HQ/<br>

**N3RDP0RT4L Website Repository**

**TODO:**
- [✓] Work on slight formating. 
- [✓] Text correction. 
- [✓] Apple fonts.
- [✓] Adding enuma device specs.
- [✓] Adding Discord HTML embeds, and icon.
- [✓] Fix-up some code.

**Our Fosscord Instance:**
http://n3rdp0rt4l.duckdns.org:9999/app

**NOTICE:**
Please read the rules to comply with the rules. You adhere to the N3RDP0RT4L terms of service, and the current rules upon using the N3RDP0RT4L services.

__**Signed Off by Austin Walker BSD 3 License...**__
